#ifndef SOLVER_HH
#define SOLVER_HH

#include <iostream>
#include <memory>
#include <string>

#include "registry.hh"

// dune-istl

template<template<class> class Solver>
auto default_solver_creator()
{
  return [] (auto m) {
    return std::make_shared<Solver<typename decltype(m)::type> >();
  };
}

struct SolverTag {};

template<class V>
struct SolverBase {
  virtual void do_something() = 0;
};

//////////////////////////////////////////////////////////////////////
//
// solver A
//

template<class V> struct SolverA : SolverBase<V> {
  virtual void do_something() {
    std::cout << "A" << std::endl;
  }
};
registry_put(SolverTag, "A", default_solver_creator<SolverA>());

//////////////////////////////////////////////////////////////////////
//
// solver B
//

template<class V> struct SolverB : SolverBase<V> {
  std::string arg;
  SolverB(std::string arg_) : arg(arg_) {}
  virtual void do_something() {
    std::cout << "B(" << arg << ")" << std::endl;
  }
};
registry_put(SolverTag, "B", [] (auto m) {
    return std::make_shared<SolverB<typename decltype(m)::type> >("dynamic");
  });

#endif // SOLVER_HH
