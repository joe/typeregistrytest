#ifndef UTILITY_HH
#define UTILITY_HH

#include <cstddef>
#include <utility>

template<class T>
struct MetaType {
  using type = T;
};

template<std::size_t p>
struct PriorityTag : PriorityTag<p-1> {
  static constexpr std::size_t value = p;
};
template<>
struct PriorityTag<0> {static constexpr std::size_t value = 0; };

void eval_args(...) {}
template<std::size_t... is, class F>
void foreach(std::index_sequence<is...>, F f)
{
  eval_args((f(std::integral_constant<std::size_t, is>{}), true)...);
}

#endif // UTILITY_HH
