#include <cassert>

#include "registry.hh"
#include "solver.hh"

int main() {
  registry_get<SolverBase, int>(SolverTag{}, "A")->do_something();
  registry_get<SolverBase, int>(SolverTag{}, "B")->do_something();
  assert((registry_get<SolverBase, int>(SolverTag{}, "C") == nullptr));
  registry_get<SolverBase, double>(SolverTag{}, "A")->do_something();
}
