#ifndef COUNTER_HH
#define COUNTER_HH

#include <cassert>
#include <typeinfo>
#include <iostream>
#include <memory>
#include <tuple>
#include <utility>

#include "utility.hh"

constexpr std::size_t maxcount = 100;

namespace {

  namespace ADL {

    struct ADLTag {};

    template<class Tag>
    constexpr std::size_t counterFunc(PriorityTag<0>, Tag, ADLTag)
    {
      return 0;
    }
  }
}

#define getCounter(Tag)                                                 \
  (counterFunc(PriorityTag<maxcount>{}, Tag{}, ::ADL::ADLTag{}))

#define incCounter(Tag)                                                 \
  namespace {                                                           \
    namespace ADL {                                                     \
      constexpr std::size_t                                             \
      counterFunc(PriorityTag<getCounter(Tag)+1> p, Tag, ADLTag)        \
      {                                                                 \
        return p.value;                                                 \
      }                                                                 \
    }                                                                   \
  }                                                                     \
  static_assert(true, "unfudge indentation")

#endif // COUNTER_HH
