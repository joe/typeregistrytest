#ifndef REGISTRY_HH
#define REGISTRY_HH

#include <cstddef>
#include <iostream>
#include <memory>
#include <string>
#include <utility>

#include "counter.hh"
#include "utility.hh"

namespace {
  template<class Tag, std::size_t index>
  struct Registry;
}

#define registry_put(Tag, id, ...)              \
  namespace {                                   \
    template<>                                  \
    struct Registry<Tag, getCounter(Tag)>       \
    {                                           \
      static auto getCreator()                  \
      {                                         \
        return __VA_ARGS__;                     \
      }                                         \
      static std::string name() { return id; }  \
    };                                          \
  }                                             \
  incCounter(Tag)

namespace {
  template<template<class> class Base, class V, class Tag>
  auto registry_get(Tag t, std::string name)
  {
    constexpr auto count = getCounter(Tag);
    std::cout << "I know " << count << " Solvers" << std::endl;
    std::shared_ptr<Base<V> > result;
    foreach(std::make_index_sequence<count>{}, [&](auto index) {
        using Reg = Registry<Tag, index>;
        if(!result && Reg::name() == name) {
          result = Reg::getCreator()(MetaType<V>{});
        }
      });
    return result;
  }
}

#endif // REGISTRY_HH
