template<size_t p>
struct PriorityTag : PriorityTag<p-1> { constexpr size_t value = p; };

template<>
struct PriorityTag<0> {constexpr size_t value = 0; };

std::pair<PriorityTag<0>, ExtraInfo> registerFunc(PriorityTag<0>);

// dune-istl

template<class V>
class SolverBasis;

class Registry {
  template<template<class> class Solver>
  void reg(std::string name);
};

template<class V>
class SolverFactory {
  SolverFactory() {
    registerISTLSolvers<V>();
  }
  shared_ptr<SolverBasis<V> > create(std::string solverName,
                                     const ParamerterTree &params);
}

template<class V>
class SolverA :
  public SolverBasis<V>
{
  SolverA(const ParamerterTree &params);
};

auto registerFunc(PriorityTag<decltype(registerFunc(PriorityTag<100>{}).first)::value+1> t) -> std::pair<decltype(t), ExtraInfo>;

// static const bool = (Registry::reg<SolverA>("A"), true);

// external Solver

template<class V>
class SolverExtA :
  public SolverBasis<V>
{
  SolverExtA(const ParamerterTree &params);
};

template<class V>
void registerExtWithISTL();

